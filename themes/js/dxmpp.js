
/**
 * @file
 * Define the baseline Drupal behaviors for the DXMPP module.
 */
(function ($) {

  // Define status constants.
  var DXMPP_STATUS_ONLINE = 2;
  var DXMPP_STATUS_AWAY = 1;
  var DXMPP_STATUS_UNAVAILABLE = 0;

  // An array of XMPP usernames.
  var dxmppUsers = new Array();

  // Set to true when the user list has been populated with AJAX.
  var dxmpp_user_list_populated = false;

  /**
   * Expand the main chat box.
   */
  Drupal.behaviors.dxmppClick = function(context) {
    $('.dxmpp-header:not(dxmppClick-processed)', context).addClass('dxmppClick-processed').bind('click', function() {
      $_box = $(this).parent();
      dxmppExpand($_box);

      // Don't pass through the click event.
      return false;
    });
  }

  /**
   * Slides the main content element up.
   */
  dxmppExpand = function($element, forceOpen) {
    var $_visible;

    // Display the main element.
    $element.show();

    if (forceOpen) {
      // We want to force it open.
      $element.children('.dxmpp-content').slideDown('slow', dxmppChangeElementTitle);
      _visible = true;
    }
    else {
      // Toggle the visibility.
      $element.children('.dxmpp-content').slideToggle('slow', dxmppChangeElementTitle);
    }

    return false;
  }

  dxmppChangeElementTitle = function() {
    $element = $(this).parent();

    // Is the main section of this area visible?
    var _visible = $element.children('.dxmpp-content').is(':visible');

    switch ($element.attr('id')) {
      case 'dxmpp-main':
        if (!_visible) {
          dxmppSetMainTitleToCount();
        }
        else {
          var _title = $('.dxmpp-title', $element).html(Drupal.settings.dxmpp.blankTitleSettings);
          $('.dxmpp-icon', _title).html('<img src="' + Drupal.settings.dxmpp.userIcon + '" height="' + Drupal.settings.dxmpp.iconHeight + '" width="' + Drupal.settings.dxmpp.iconWidth + '" />');
          $('.dxmpp-name', _title).text(dxmppMainUserName());
        }
        break;
      case 'dxmpp-chatbox':
//         // @TODO: discover the real user here (from the selected tab)...
//         console.log($('.ui-tabs-selected a', this));
//         var _title = $('.dxmpp-title', $element).html(Drupal.settings.dxmpp.blankChatboxTitle).addClass('dxmpp-status-available');
//         $('.dxmpp-icon', _title).html('<img src="' + Drupal.settings.dxmpp.userIcon + '" height="' + Drupal.settings.dxmpp.iconHeight + '" width="' + Drupal.settings.dxmpp.iconWidth + '" />');
//         $('.dxmpp-name', _title).text(dxmppMainUserName());
//         // @TODO: Use settings for status here...
//         $('.dxmpp-title-status', _title).text('Available');
        break;
    }
  }

  /**
   * Open a new chatbox when clicking on a user.
   */
  Drupal.behaviors.dxmppUserLink = function(context) {
    $('.dxmpp-roster-item-inner-wrapper:not(dxmppUserLink-processed)', context).addClass('dxmppUserLink-processed', context).bind('click', function() {
      // Grab the user info.
      var user = $(this).parent().data('dxmpp_user');

      dxmppOpenChat(user);

      // Halt further processing.
      return false;
    });
  }

  dxmppChatIdFromJid = function(jid) {
    return '#dxmpp-chat-' + dxmppJidToId(jid);
  }

  dxmppGetTime = function(_date) {
    if (!_date) {
      _date = new Date();
    }

    var hour   = _date.getHours();
    var minute = _date.getMinutes();
    var ap = "am";
    if (hour   > 11) { ap = "pm";             }
    if (hour   > 12) { hour = hour - 12;      }
    if (hour   == 0) { hour = 12;             }
    if (minute < 10) { minute = "0" + minute; }
    var timeString = hour +
                      ':' +
                      minute +
                      " " +
                      ap;
    return timeString;
  }

  /**
   * Bind the <enter> key with sending the message.
   */
  Drupal.behaviors.dxmppChatInput = function(context) {
    $('.dxmpp-chat-input', context).live('keypress', function(event) {
      var jid = $(this).parent().data('jid');

      if (event.which === 13) {
        event.preventDefault();
        var body = $(this).val();

        if (body !== '') {
          var message = $msg({ to: jid, 'type': 'chat' }).c('body').t(body).up().c('active', {xmlns: 'http://jabber.org/protocol/chatstates'});
          dxmppConnection.send(message);

          dxmppAddMessage($(this).parent(), jid, body, true);

          $(this).val('');
          $(this).parent().data('composing', false);
        }
        return false;
      }
      else {
        var composing = $(this).parent().data('composing');
        if (!composing) {
          var notify = $msg({to: jid, 'type': 'chat'}).c('composing', {xmlns: 'http://jabber.org/protocol/chatstates'});
          dxmppConnection.send(notify);

          $(this).parent().data('composing', true);
        }
      }
    });
  }

  dxmppAddMessage = function($element, jid, body, fromUser) {
    $last = $('.dxmpp-chat-message:last', $element);
    if ($last.length > 0) {
      if ((fromUser && $last.hasClass('dxmpp-me')) || (!fromUser && !$last.hasClass('dxmpp-me'))) {
        _text = Drupal.settings.dxmpp.blankChatText;
        $(_text).text(body).appendTo($('.dxmpp-chat-text', $last));
        dxmppScrollChat(dxmppJidToId(jid));

        return true;
      }
    }
    if (!$element.data('msg_count')) {
      $element.data('msg_count', 0);
    }
    msg_count = $element.data('msg_count');
    $element.data('msg_count', ++msg_count);

    var _id = 'dxmpp-message-' + dxmppJidToId(jid) + '--' + msg_count;
    _message = Drupal.settings.dxmpp.blankChatMessage;
    $_msg = $(_message).attr('id', _id).appendTo($('.dxmpp-chat-messages', $element));
    if (fromUser) {
      $_msg.addClass('dxmpp-me');
      $('.dxmpp-chat-name', $_msg).text(dxmppMainUserName());
    }
    else {
      $('.dxmpp-chat-name', $_msg).text(dxmppJidToUsername(jid));
    }
    _text = Drupal.settings.dxmpp.blankChatText;
    $('.dxmpp-chat-text', $_msg).html($(_text).text(body));
    $('.dxmpp-chat-time', $_msg).text(dxmppGetTime());

    dxmppScrollChat(dxmppJidToId(jid));
    return true;
  }

  dxmppScrollChat = function(jid_id) {
    var div = $('#dxmpp-chat-' + jid_id + ' .dxmpp-chat-messages').get(0);
    div.scrollTop = div.scrollHeight;
  }

  /**
   * Returns the unique ID for a user, derived from the Drupal $user->uid.
   */
  dxmpp_roster_id = function(user) {
    return 'dxmpp-roster-id-' + dxmppJidToId(user.uid);
  }

  /**
   * Add and populate a user roster item.
   */
  dxmpp_add_user_to_roster = function(user) {
    // Create unique ID.
    var _id = dxmpp_roster_id(user);
    var _blank = Drupal.settings.dxmpp.blankRosterItem;

    var $roster = $('#dxmpp-roster ul li');

    // Remove any old instances of this user.
    $('#' + _id).remove();

    var inserted = false;
    if ($roster.length > 0) {
      $roster.each(function() {
        var presence = dxmppPresenceValue($(this));
        var username = $(this).find('.dxmpp-username').text();

        if (user.presence > presence) {
          $(_blank).attr('id', _id).insertBefore($(this));
          inserted = true;
          return false;
        }
        else if ((user.presence == presence) && (user.username < username)) {
          $(_blank).attr('id', _id).insertBefore($(this));
          inserted = true;
          return false;
        }
      });
    }
    if (!inserted) {
      $(_blank).attr('id', _id).appendTo($('#dxmpp-roster ul'));
    }

    var $item = $('#' + _id);

    // Store the user data.
    $item.data('dxmpp_user', user);

    // Add status.
    $item.addClass(dxmppPresenceClass(user.presence));
    // Add username. @TODO: use drupal username.
    $('.dxmpp-username', $item).text(user.username);
    // Add icon. @TODO: use actual icon.
    user.icon = Drupal.settings.dxmpp.userpics[user.username];
    $('.dxmpp-icon', $item).html('<img src="' + user.icon + '" height="' + Drupal.settings.dxmpp.iconHeight + '" width="' + Drupal.settings.dxmpp.iconWidth + '" />');

    // Bind click event to open new chat.
    Drupal.attachBehaviors($item.get(0));

    $('#dxmpp-roster ul li').removeClass('last').removeClass('first');
    $('#dxmpp-roster ul li:first').addClass('first');
    $('#dxmpp-roster ul li:last').addClass('last');

    dxmppUsers[user.jid] = user;
  }

  var _dxmppUserCount = 0;

  dxmppSetUserCount = function(count) {
    _dxmppUserCount = count;
  }

  dxmppGetUserCount = function() {
    return _dxmppUserCount;
  }

  dxmppSetMainTitleToCount = function() {
    var count = dxmppGetUserCount();
    $('#dxmpp-main .dxmpp-title').html(Drupal.formatPlural(count, Drupal.settings.dxmpp.textFriendsSingular, Drupal.settings.dxmpp.textFriendsPlural));
  }

  /**
   * Get a roster item's status.
   */
  dxmppPresenceValue = function($element) {
    if ($element.hasClass('dxmpp-status-online')) {
      return DXMPP_STATUS_ONLINE;
    }
    else if ($element.hasClass('dxmpp-status-away')) {
      return DXMPP_STATUS_AWAY;
    }
    return DXMPP_STATUS_UNAVAILABLE;
  }

  dxmppPresenceClass = function(presence) {
    switch (presence) {
      case DXMPP_STATUS_ONLINE:
      case 'online':
        return 'dxmpp-status-online';
      case DXMPP_STATUS_AWAY:
      case 'away':
        return 'dxmpp-status-away';
    }
    return 'dxmpp-status-unavailable';
  }

  /** *******************
   **  Strophe Interface
   ** *******************/

  // The global connection object.
  var dxmppConnection;

  /**
   * Make our initial connection, which is maintained during the page session.
   */
  dxmppConnect = function() {
    // Connect to our XMPP server.
    var conn = new Strophe.Connection(Drupal.settings.dxmpp.stropheConnection);

    // Connect to the appropriate user.
    // @TODO: Salt the pw prior to here.
    conn.connect(Drupal.settings.dxmpp.jid, Drupal.settings.dxmpp.password, function(status) {
      if (status === Strophe.Status.CONNECTED) {
        // Success!
        $(document).trigger('connected');
      }
      else {
        // Wot?
        $(document).trigger('disconnected');
      }
    });

    // Set our global connection object.
    dxmppConnection = conn;
  }

  /**
   * Populate the roster when we first make our connection.
   */
  dxmppOnRoster = function(iq) {
    dxmppSetUserCount($(iq).find('item').length);
    dxmppSetMainTitleToCount();

    // Add each item of the roster.
    $(iq).find('item').each(function() {
      var jid = $(this).attr('jid');
      var name = $(this).attr('name') || dxmppJidToUsername(jid);

      var user = {
        username: name || dxmppJidToId(jid),  // The Drupal username.
        jid: jid,                             // The full XMPP user ID.
        uid: jid,                             // The Drupal uid.
        icon: Drupal.settings.dxmpp.userpics[name], // The user picture.
        presence: DXMPP_STATUS_UNAVAILABLE,   // The jabber presence status.
        dversion: 0                           // Which version of Drupal data.
      };

      dxmpp_add_user_to_roster(user);
    });

    // Set up the presence handler and send our initial presence.
    dxmppConnection.addHandler(dxmppOnPresence, null, "presence");
    dxmppConnection.send($pres());
  }

  /**
   * Respond to roster changes, adding & dropping items.
   */
  dxmppOnRosterChange = function(iq) {
    $(iq).find('item').each(function() {
      var sub = $(this).attr('subscription');
      var jid = $(this).attr('jid');
      var name = $(this).attr('name') || dxmppJidToUsername(jid);

      if (sub === 'remove') {
        // Remove the contact.
        $('#' + dxmpp_roster_id(jid)).remove();
      }
      else {
        // Add or modify the contact.
        var user = {
          username: name || dxmppJidToId(jid),  // The Drupal username.
          jid: jid,                             // The full XMPP user ID.
          uid: jid,                             // The Drupal uid.
          icon: Drupal.settings.dxmpp.userpics[name], // The user picture.
          presence: DXMPP_STATUS_UNAVAILABLE,   // The jabber presence status.
          dversion: 0                           // Which version of Drupal data.
        };

        dxmpp_add_user_to_roster(user);
      }
    });
    return true;
  }

  dxmppOnPresence = function(presence) {
    var presenceType = $(presence).attr('type');
    var from = $(presence).attr('from');

    if (presenceType !== 'error') {
      var jid = Strophe.getBareJidFromJid(from);
      var jid_id = dxmppJidToId(from);
      if (jid in dxmppUsers) {
        if (presenceType === 'unavailable') {
          dxmppUsers[jid].presence = DXMPP_STATUS_UNAVAILABLE;
        }
        else {
          var show = $(presence).find('show').text();
          if (show === '' || show === 'chat') {
            dxmppUsers[jid].presence = DXMPP_STATUS_ONLINE;
          }
          else {
            dxmppUsers[jid].presence = DXMPP_STATUS_AWAY;
          }
        }
        // Drop and re-add the old roster item; dynamically resort the roster.
        dxmpp_add_user_to_roster(dxmppUsers[jid]);
        dxmppSetChatTitleContent();
      }
    }

    // Ensure we continue to receive and process presence requests.
    return true;
  }

  dxmppGetActiveChatUser = function() {
    var _jid = $($('#dxmpp-chatbox .ui-tabs-selected a').attr('href')).data('jid');
    if (_jid && (_jid != dxmppConnection.jid)) {
      return dxmppUsers[_jid];
    }
  }

  dxmppSetChatTitleContent = function() {
    var user = dxmppGetActiveChatUser();
    if (user) {
      var _title = $('#dxmpp-chatbox .dxmpp-title').html(Drupal.settings.dxmpp.blankChatboxTitle).addClass('dxmpp-status-available');
      $('.dxmpp-icon', _title).html('<img src="' + Drupal.settings.dxmpp.userpics[user.username] + '" height="' + Drupal.settings.dxmpp.iconHeight + '" width="' + Drupal.settings.dxmpp.iconWidth + '" />');
      $('.dxmpp-name', _title).text(dxmppJidToUsername(user.jid));
      $('.dxmpp-title-status', _title).text(Drupal.settings.dxmpp.statusText[user.presence]);
    }
  }

  dxmppOpenChat = function(user) {
    // Epand the chat box.
    dxmppExpand($('#dxmpp-chatbox'), true);

    var jid = user.jid;
    var name = user.username;

    var chat_id = dxmppChatIdFromJid(jid);

    // Select or add the chat tab.
    if ($(chat_id).length > 0) {
      $('#dxmpp-chatbox .dxmpp-content').tabs('select', chat_id);
    }
    else {
      $('#dxmpp-chatbox .dxmpp-content').tabs('add', chat_id, name).tabs('select', chat_id);
      $(chat_id).append(Drupal.settings.dxmpp.blankChatbox);
      $(chat_id).data('dxmpp_user', user);
      $(chat_id).data('jid', jid);
    }

    // Switch the focus to the correct chatbox tab.
    $('textarea', chat_id).focus();

    // Set the main title to display icon & username.
    dxmppSetChatTitleContent();

    return true;
  }

  dxmppOnMessage = function(message) {
    var jid = Strophe.getBareJidFromJid($(message).attr('from'));
    var jid_id = dxmppJidToId(jid);
    var name = dxmppJidToUsername(jid);

    var user = {
      username: name,  // The Drupal username.
      jid: jid,                             // The full XMPP user ID.
      uid: jid,                             // The Drupal uid.
      icon: Drupal.settings.dxmpp.userpics[name], // The user picture.
      presence: DXMPP_STATUS_UNAVAILABLE,   // The jabber presence status.
      dversion: 0                           // Which version of Drupal data.
    };

    // Open the chat for this user.
    dxmppOpenChat(dxmppUsers[jid]);

    var composing = $(message).find('composing');
    if (composing.length > 0) {
      $('.dxmpp-chat-events', dxmppChatIdFromJid(jid)).text(dxmppUsers[jid].username + ' is typing...');
    }

    // First look for an HTML encoded body.
    var body = $(message).find("html > body");

    if (body.length === 0) {
      // This is not an HTML encoded body. Look for a plain text body.
      body = $(message).find('body');
      if (body.length > 0) {
        // The text is our body.
        body = body.text();
      }
      else {
        // This is a blank message.
        body = null;
      }
    }
    else {
      body = body.contents();

      var span = $("<span></span");
      body.each(function() {
        if (document.importNode) {
          $(document.importNode(this, true)).appendTo(span);
        }
        else {
          // IE workaround.
          span.append(this.xml);
        }
      });

      body = span;
    }

    if (body) {
      $('.dxmpp-chat-events', dxmppChatIdFromJid(jid)).text('');
      // Add the new message.
      dxmppAddMessage($(dxmppChatIdFromJid(jid)), jid, body, false);
    }

    // We need to continue processing future messages.
    return true;
  }

  $(document).bind('connected', function() {
    // When we first connect, grab the most current roster.
    var iq = $iq({type: 'get'}).c('query', {xmlns: 'jabber:iq:roster'});

    // Grab the roster and trigger the dxmppOnRoster function.
    dxmppConnection.sendIQ(iq, dxmppOnRoster);

    // Respond to changes in the roster.
    dxmppConnection.addHandler(dxmppOnRosterChange, "jabber:iq:roster", "iq", "set");

    // Display received messages.
    dxmppConnection.addHandler(dxmppOnMessage, null, "message", "chat");
  });

  $(document).bind('disconnected', function() {
  });

  $(document).ready(function() {
    dxmppSetUserCount(0);
    dxmppSetMainTitleToCount();
    dxmppConnect();
  });

  /**
   * Convert the characters of an XMPP/Jabber ID to those suitable for a CSS id.
   */
  dxmppJidToId = function(jid) {
    return Strophe.getBareJidFromJid(jid).replace(/@/g, '-').replace(/\./g, '-');
  }

  /**
   * Return just the username portion of the full XMPP ID.
   */
  dxmppJidToUsername = function(jid) {
    return Strophe.getNodeFromJid(jid);
  }

  /**
   * Return the XMPP username of the logged in user.
   */
  dxmppMainUserName = function() {
    return dxmppJidToUsername(dxmppConnection.jid);
  }

  /**
   * Initialize chat area.
   */
  Drupal.behaviors.dxmppInitializeChatArea = function(context) {
    // Initialize tabs and make them sortable.
    $('#dxmpp-chatbox .dxmpp-content:not(.dxmppInitializeChatArea-processed)', context).addClass('dxmppInitializeChatArea-processed').tabs().find('.ui-tabs-nav').sortable({axis: 'x'});
  }

  Strophe.log = function (level, msg) {
    if (level) {
      dxmppDebug('Strophe: ' + level + ' - "' + msg + '"');
    }
  };

  dxmppDebug = function(msg) {
    if (window.console && console.log) {
      console.log(msg);
    }
  }

})(jQuery);
