<?php

define('DXMPP_STATUS_ONLINE', 2);
define('DXMPP_STATUS_AWAY', 1);
define('DXMPP_STATUS_UNAVAILABLE', 0);

/**
 * @file
 * Define theme and theme preprocess functions for DXMPP.
 */

/**
 * Add the entire chatbox.
 */
function theme_dxmpp_chat() {
  global $user;

  // Load the XMPP user/password. If there is none, then register & store.
  dxmpp_user_load($user);

  // If we have no associated record, then return.
  if (!$user->dxmpp) {
    return '';
  }

  // Add the strophe library.
  drupal_add_js(dxmpp_strophe_path() .'/strophe.js');

  // Optionally add the flxHR library.
  if (dxmpp_variable_get('use_flxhr')) {
    drupal_add_js(dxmpp_flxhr_path() .'/flxhr.js');
    drupal_add_js(dxmpp_strophe_path() .'/strophe.flxhr.js');
  }

  // Add jQuery Tabs & Sortabe.
  jquery_ui_add('ui.tabs');
  jquery_ui_add('ui.sortable');

  // Add our listeners and other custom js & CSS.
  drupal_add_js(drupal_get_path('module', 'dxmpp') .'/themes/js/dxmpp.js');
  drupal_add_css(drupal_get_path('module', 'dxmpp') .'/themes/dxmpp.css');

  if (module_exists('imagecache') && dxmpp_variable_get('icon_imagecache')) {
    $user_icon = imagecache_create_url(dxmpp_variable_get('icon_imagecache'), $user->picture);
  }
  else {
    $user_icon = url($user->picture);
  }

  // Add JS settings.
  $settings = array(
    'getUserListURL' => url('dxmpp/user-list'),
    'blankRosterItem' => theme('dxmpp_blank_roster_item'),
    'blankChatbox' => theme('dxmpp_blank_chatbox'),
    'blankChatMessage' => theme('dxmpp_blank_chat_message'),
    'blankChatText' => theme('dxmpp_blank_chat_text'),
    'blankTitleSettings' => theme('dxmpp_blank_title_settings'),
    'blankChatboxTitle' => theme('dxmpp_blank_chatbox_title'),
    'textFriendsSingular' => t(dxmpp_variable_get('text_friends_singular')),
    'textFriendsPlural' => t(dxmpp_variable_get('text_friends_plural')),
    'statusText' => array(
      DXMPP_STATUS_ONLINE => t('Available'),
      DXMPP_STATUS_AWAY => t('Away'),
      DXMPP_STATUS_UNAVAILABLE => t('Unavailable'),
    ),
    'userIcon' => $user_icon,
    // @TODO: Use real user icon.
//     'userIcon' => url('sites/default/files/imagecache/avatar/pictures/picture-1.png'),
    'iconHeight' => dxmpp_variable_get('icon_height'),
    'iconWidth' => dxmpp_variable_get('icon_width'),
    'rosterEmptyMessage' => theme('dxmpp_roster_empty_message'),
    'stropheConnection' => dxmpp_variable_get('server'),
    'jid' => $user->dxmpp->xmpp_username .'@'. dxmpp_variable_get('domain'),
    'password' => $user->dxmpp->xmpp_password,
    'userpics' => dxmpp_roster_userpics($user),
//     'jid' => 'advomatic@'. dxmpp_variable_get('domain'),
//     'password' => dxmpp_variable_get('debug_password'),
  );
  drupal_add_js(array('dxmpp' => $settings), 'setting');

  // Display the chatbox and roster.
  $output = '<div id="dxmpp">';
  $output .=  theme('dxmpp_roster_wrapper');
  $output .= theme('dxmpp_chatbox_wrapper');
  $output .= '</div>';
  return $output;
}

/**
 * The main roster box on the bottom right. Expands to display chat roster.
 */
function theme_dxmpp_roster_wrapper() {
  $output = '<div id="dxmpp-main" class="dxmpp-box dxmpp-main dxmpp-collapsed">';
  $output .= theme('dxmpp_title', t('Connecting to chat server...'));
  $output .= theme('dxmpp_content', theme('dxmpp_blank_roster'));
  $output .= '</div>';
  return $output;
}

/**
 * The chatbox. Expands to display tabbed chats.
 */
function theme_dxmpp_chatbox_wrapper() {
  $output = '<div id="dxmpp-chatbox" class="dxmpp-box dxmpp-chatbox dxmpp-collapsed">';
  $output .= theme('dxmpp_title', t('Chatbox'));
  $output .= theme('dxmpp_content', '<ul></ul>');
  $output .= '</div>';
  return $output;
}

function theme_dxmpp_title($title = '') {
  $translucent = theme('dxmpp_translucent_background');
  return <<<OUTPUT
    <div class="dxmpp-header"><div class="dxmpp-title">$title</div></div>
    $translucent
OUTPUT;
}

function theme_dxmpp_roster_empty_message() {
  return '<div id="dxmpp-empty-list-message" class="dxmpp-empty-list-message">'. t(dxmpp_variable_get('roster_empty_message')) .'</div>';
}

function theme_dxmpp_blank_roster() {
  return '<div id="dxmpp-roster" class="dxmpp-roster"><ul></ul></div>';
}

function theme_dxmpp_expand_button() {
  return '<div class="dxmpp-expand"></div>';
}

/**
 * This is an empty structure to be built in jQuery.
 */
function theme_dxmpp_blank_roster_item() {
  $output = <<<OUTPUT
  <li class="dxmpp-roster-item">
    <div class="dxmpp-roster-item-inner-wrapper">
      <div class="dxmpp-status"></div>
      <div class="dxmpp-username"></div>
      <div class="dxmpp-icon"></div>
    </div>
  </li>
OUTPUT;
  return $output;
}

function theme_dxmpp_blank_chatbox() {
  return <<<OUTPUT
    <div class="dxmpp-chat-messages"></div>
    <div class="dxmpp-chat-events"></div>
    <textarea class="dxmpp-chat-input" />
OUTPUT;
}

function theme_dxmpp_blank_chat_message() {
  return <<<OUTPUT
    <div class="dxmpp-chat-message">
      <div class="dxmpp-chat-text"></div>
      <div class="dxmpp-chat-message-info">
        <div class="dxmpp-chat-name"></div>
        <div class="dxmpp-chat-time"></div>
      </div>
    </div>
OUTPUT;
}

function theme_dxmpp_blank_chat_text() {
  return <<<OUTPUT
        <div class="dxmpp-chat-subtext">
        </div>
OUTPUT;
}

function theme_dxmpp_blank_title_settings() {
  global $user;
  $settings = l(t('Settings'), 'user/'. $user->uid, array('attributes' => array('class' => 'dxmpp-settings-settings')));
  $privacy = l(t('Privacy'), 'user/'. $user->uid, array('attributes' => array('class' => 'dxmpp-settings-privacy')));
  $help = l(t('Help'), 'user/'. $user->uid, array('attributes' => array('class' => 'dxmpp-settings-help')));
  $online_value = DXMPP_STATUS_ONLINE;
  $away_value = DXMPP_STATUS_AWAY;
  $unavailable_value = DXMPP_STATUS_UNAVAILABLE;
  $online = t('Available');
  $away = t('Away');
  $unavailable = t('Offline');
  return <<<OUTPUT
    <div class="dxmpp-main-title-content">
      <div class="dxmpp-icon"></div>
      <div class="dxmpp-name"></div>
      <div class="dxmpp-settings-status">
        <select>
          <option value="$online_value">$online</option>
          <option value="$away_value">$away</option>
          <option value="$unavailable_value">$unavailable</option>
        </select>
      </div>
      <div class="dxmpp-settings">
        $settings
        $privacy
        $help
      </div>
    </div>
OUTPUT;
}

function theme_dxmpp_blank_chatbox_title() {
  return <<<OUTPUT
    <div class="dxmpp-chatbox-title-content">
      <div class="dxmpp-icon"></div>
      <div class="dxmpp-name"></div>
      <div class="dxmpp-title-status"></div>
    </div>
OUTPUT;
}

function theme_dxmpp_main_expanded() {
}

function theme_dxmpp_translucent_background() {
//   return '<div class="dxmpp-translucent"></div>';
}

function theme_dxmpp_content($output = '') {
  return '<div class="dxmpp-content">'. $output .'</div>';
}
