<?php

/**
 * @file
 * Page callbacks for AJAX/JSON data.
 */

/**
 * Page callback for dxmpp/user-list.
 */
function dxmpp_user_list_json() {
  $friends = json_decode($_GET['friends'], TRUE);
  $items = array();
  if (is_array($friends)) {
    foreach ($friends as $friend) {
      $account = user_load(array('name' => $friend));
      $items[] = theme('dxmpp_user_item', $account->uid);
    }
  }
  if (!empty($items)) {
    $output = theme('item_list', $items);
  }
  else {
    $output = theme('dxmpp_empty_list_message');
  }
  drupal_json(array('output' => $output, 'status' => 1, 'friends' => $friends));
}
