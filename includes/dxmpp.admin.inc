<?php

/**
 * @file
 * Administrative functions to configure DXMPP.
 */

/**
 * Callback for admin/settings/dxmpp.
 */
function dxmpp_settings_form() {
  // Autodiscover the path to the strophe and flxhr libraries.
  dxmpp_strophe_path();
  dxmpp_flxhr_path();

  $form = array();
  $form[dxmpp_variable_name('strophe_path')] = array(
    '#type' => 'textfield',
    '#title' => t('Strophe library'),
    '#description' => t("You need to download the latest stable release of the !library and extract the entire folder into sites/all/libraries. The path will be automatically discovered; you may override the directory path here if you wish to use another version; do not add slashes before or after the path.", array('!library' => l(t('Strophe library'), 'http://code.stanziq.com/strophe/', array('attributes' => array('target' => '_blank'))))),
    '#default_value' => dxmpp_variable_get('strophe_path'),
  );
  $form[dxmpp_variable_name('domain')] = array(
    '#type' => 'textfield',
    '#title' => t('XMPP Server domain'),
    '#description' => t('Please enter the domain for your XMPP server, which is used to authenticate XMPP users. For instance, you might enter %example.', array('%example' => 'xmpp.example.com')),
    '#default_value' => dxmpp_variable_get('domain'),
  );
  $form[dxmpp_variable_name('server')] = array(
    '#type' => 'textfield',
    '#title' => t('XMPP Bosh URL'),
    '#description' => t("Please enter the BOSH URL of the XMPP server, which is used for the browser to communicate with the XMPP server. For instance, you might use %example. Unless you are using the !flXHR browser cross-domain tool, you will probably need to set up a reverse proxy. Please read the !documentation for further assistance.", array('%example' => 'http://jabber.example.com/http-bind/', '!flXHR' => l(t('flXHR'), 'http://flxhr.flensed.com/', array('attributes' => array('target' => '_blank'))), '!documentation' => l(t('online documentation'), 'http://drupal.org/project/dxmpp', array('attributes' => array('target' => '_blank'))))),
    '#default_value' => dxmpp_variable_get('server'),
  );
  $form['flxhr'] = array(
    '#type' => 'fieldset',
    '#title' => t('flXHR library'),
    '#collapsible' => TRUE,
    '#collapsed' => !dxmpp_variable_get('use_flxhr'),
  );
  $form['flxhr'][dxmpp_variable_name('use_flxhr')] = array(
    '#type' => 'checkbox',
    '#title' => t('Use flXHR'),
    '#default_value' => dxmpp_variable_get('use_flxhr'),
    '#description' => t('If you do not have a reverse proxy to the XMPP server set up, you will probably need to install and use the !flXHR browser cross-domain tool.', array('!flXHR' => l(t('flXHR'), 'http://flxhr.flensed.com/', array('attributes' => array('target' => '_blank'))))),
  );
  $form['flxhr'][dxmpp_variable_name('flxhr_path')] = array(
    '#type' => 'textfield',
    '#title' => t('flXHR library'),
    '#description' => t("Unless you have set up a reverse proxy, you may need to download the latest stable release of the !library and extract the entire folder into sites/all/libraries. The path will be automatically discovered; you may override the directory path here if you wish to use another version; do not add slashes before or after the path.", array('!library' => l(t('flXHR library'), 'http://flxhr.flensed.com/', array('attributes' => array('target' => '_blank'))))),
    '#default_value' => dxmpp_variable_get('flxhr_path'),
  );

  $form[dxmpp_variable_name('icon_imagecache')] = array(
    '#type' => 'select',
    '#title' => t('Icon ImageCache preset'),
    '#default_value' => dxmpp_variable_get('icon_imagecache'),
    '#description' => t('Please select the ImageCache preset to use to resize user icons for the chat roster. Note that they will also be resized by the browser to %px regardless of this setting, unless overridden by the theme or CSS. You can !administer to add new presets. Requires the !imagecache module to be installed.', array('%px' => t('@w x @h pixels', array('@w' => dxmpp_variable_get('icon_width'), '@h' => dxmpp_variable_get('icon_height'))), '!administer' => l(t('manage ImageCache presets'), 'admin/build/imagecache'), '!imagecache' => l(t('ImageCache'), 'http://drupal.org/project/imagecache', array('attributes' => array('target' => '_blank')))))
  );
  $options = array('' => t('<none>'));
  if (module_exists('imagecache')) {
    foreach (imagecache_presets() as $preset) {
      $options[$preset['presetname']] = $preset['presetname'];
    }
  }
  else {
    $form[dxmpp_variable_name('icon_imagecache')]['#disabled'] = TRUE;
  }
  $form[dxmpp_variable_name('icon_imagecache')]['#options'] = $options;

  $form[dxmpp_variable_name('use_dxmpp_auth')] = array(
    '#type' => 'checkbox',
    '#title' => t('Use DXMPP Authorization'),
    '#default_value' => dxmpp_variable_get('use_dxmpp_auth'),
    '#description' => t("If checked, we'll use the DXMPP Authorization module to register new users. This requires both that we have that module enabled, and that the XMPP server be on the same local server as the Drupal installation."),
  );
  if (!module_exists('dxmpp_auth')) {
    $form[dxmpp_variable_name('use_dxmpp_auth')]['#disabled'] = TRUE;
  }

//   $form[dxmpp_variable_name('admin_username')] = array(
//     '#type' => 'textfield',
//     '#title' => t('Admin username'),
//     '#default_value' => dxmpp_variable_get('admin_username'),
//     '#description' => t('Enter the XMPP server administrative username here.'),
//   );
//   $form[dxmpp_variable_name('admin_password')] = array(
//     '#type' => 'textfield',
//     '#title' => t('Admin password'),
//     '#default_value' => dxmpp_variable_get('admin_password'),
//     '#description' => t('Enter the XMPP server administrative password here.'),
//   );
//
//   // @TODO: Remove this setting when we can register users automatically.
//   $form[dxmpp_variable_name('debug_password')] = array(
//     '#type' => 'textfield',
//     '#title' => t('Debug Password'),
//     '#default_value' => dxmpp_variable_get('debug_password'),
//   );

  return system_settings_form($form);
}

/**
 * Autodiscover a system path containing a library.
 *
 * This will attempt to automatically discover the system path to the folder
 * containing the desired library. It will first search in sites/example.com/libraries, then
 * sites/all/libraries.
 *
 * @param string $match
 *   The regex pattern to search for.
 * @return string
 *   The path to the system folder containing that library.
 */
function _dxmpp_autodiscover_path($match) {
  $path = '';
  $files = drupal_system_listing($match, 'libraries', 'basename', 0);
  if (isset($files['strophe.js'])) {
    $path = dirname($files['strophe.js']->filename);
  }
  return $path;
}
